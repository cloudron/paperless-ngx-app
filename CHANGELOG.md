[0.5.0]
* Initial version

[0.5.1]
* Update description and tagline

[0.6.0]
* Use ASGI gunicorn server to support websockets

[0.7.0]
* Install all OCR languages in the app image

[0.8.0]
* **Important**: This package now uses the [Paperless-ngx](https://github.com/paperless-ngx/paperless-ngx) project
* Update Paperless-ngx to 1.6.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/ngx-1.6.0)

[0.9.0]
* Update Paperless-ngx to 1.7.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/ngx-1.7.0)
* global drag'n'drop @shamoon (#283)
* Fix: download buttons should disable while waiting @shamoon (#630)
* Update checker @shamoon (#591)
* Show prompt on password-protected pdfs @shamoon (#564)
* Filtering query params aka browser navigation for filtering @shamoon (#540)
* Clickable tags in dashboard widgets @shamoon (#515)
* Add bottom pagination @shamoon (#372)
* Feature barcode splitter @gador (#532)
* App loading screen @shamoon (#298)
* Use progress bar for delayed buttons @shamoon (#415)
* Add minimum length for documents text filter @shamoon (#401)
* Added nav buttons in the document detail view @GruberViktor (#273)
* Improve date keyboard input @shamoon (#253)

[1.0.0]
* Update Paperless-ngx to 1.7.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.7.1)
* mobile friendlier manage pages @shamoon (#873)
* Use semver for release process @stumpylog (#851)
* Feature barcode tiff support @gador (#766)

[1.1.0]
* Update Paperless-ngx to 1.8.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.8.0)
* Feature use env vars in pre post scripts @ziprandom (#1154)
* frontend task queue @shamoon (#1020)
* Fearless scikit-learn updates @stumpylog (#1082)
* Adds support for Docker secrets @stumpylog (#1034)
* make frontend timezone un-aware @shamoon (#957)
* Change document thumbnails to WebP @stumpylog (#1127)
* Fork django-q to update dependencies @stumpylog (#1014)
* Fix: Rework query params logic @shamoon (#1000)
* Enhancement: show note on language change and offer reload @shamoon (#1030)
* Include error information when Redis connection fails @stumpylog (#1016)
* frontend settings saved to database @shamoon (#919)
* Add "Created" as additional (optional) parameter for post_documents @eingemaischt (#965)
* Convert Changelog to markdown, auto-commit future changelogs @qcasey (#935)
* allow all ASN filtering functions @shamoon (#920)
* gunicorn: Allow IPv6 sockets @vlcty (#924)
* initial app loading indicators @shamoon (#899)

[1.2.0]
* Update Paperless-ngx to 1.9.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.9.0)

[1.2.1]
* Update Paperless-ngx to 1.9.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.9.1)
* Bugfix: Fixes missing OCR mode skip noarchive @stumpylog (#1645)
* Fix reset button padding on small screens @shamoon (#1646)

[1.2.2]
* Update Paperless-ngx to 1.9.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.9.2)
* Bugfix: Allow `PAPERLESS_OCR_CLEAN=none`

[1.3.0]
* Update Paperless-ngx to 1.10.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.10.0)
* Feature: Capture stdout & stderr of the pre/post consume scripts @stumpylog (#1967)
* Feature: Allow running custom container initialization scripts @stumpylog (#1838)
* Feature: Add more file name formatting options @stumpylog (#1906)
* Feature: 1.9.2 UI tweaks @shamoon (#1886)
* Feature: Optional celery monitoring with Flower @stumpylog (#1810)
* Feature: Save pending tasks for frontend @stumpylog (#1816)
* Feature: Improved processing for automatic matching @stumpylog (#1609)
* Feature: Transition to celery for background tasks @stumpylog (#1648)
* Feature: UI Welcome Tour @shamoon (#1644)
* Feature: slim sidebar @shamoon (#1641)
* change default matching algo to auto and move to constant @NiFNi (#1754)
* Feature: Enable end to end Tika testing in CI @stumpylog (#1757)
* Feature: frontend update checking settings @shamoon (#1692)
* Feature: Upgrade to qpdf 11, pikepdf 6 & ocrmypdf 14 @stumpylog (#1642)

[1.3.1]
* Update Paperless-ngx to 1.10.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.10.1)
* Feature: Allows documents in WebP format @stumpylog (#1984)
* Fix: frontend tasks display in 1.10.0 @shamoon (#2073)
* Bugfix: Custom startup commands weren't run as root @stumpylog (#2069)
* Bugfix: Add libatomic for armv7 compatibility @stumpylog (#2066)
* Bugfix: Don't silence an exception when trying to handle file naming @stumpylog (#2062)
* Bugfix: Some tesseract languages aren't detected as installed. @stumpylog (#2057)

[1.3.2]
* Update Paperless-ngx to 1.10.2
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.10.2)
* Bugfix: Language code checks around two part languages @stumpylog (#2112)
* Bugfix: Redis socket compatibility didn't handle URLs with ports @stumpylog (#2109)
* Bugfix: Incompatible URL schemes for socket based Redis @stumpylog (#2092)
* Fix doc links in contributing @tooomm (#2102)

[1.4.0]
* Update Paperless-ngx to 1.11.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.11.0)
* Feature: frontend paperless mail @shamoon (#2000)
* Feature: Ability to consume mails and eml files @p-h-a-i-l (#848)
* Chore: Downgrade hiredis to 2.0.0 @stumpylog (#2262)
* Add ability to provide the configuration file path using an env variable @hashworks (#2241)
* Feature: Adds option to allow a user to export directly to a zipfile @stumpylog (#2004)
* Feature: Adds PaperlessTask admin page interface @stumpylog (#2184)
* Feature: speed up frontend by truncating content @shamoon (#2028)
* Feature: Allow bulk download API to follow file name formatting @stumpylog (#2003)
* Feature: Bake NLTK into Docker image @stumpylog (#2129)
* Feature: frontend paperless mail @shamoon (#2000)
* Feature: Ability to consume mails and eml files @p-h-a-i-l (#848)
* Bugfix: Handle RTL languages better @stumpylog (#1665)
* Fixed typo in docs @mendelk (#2256)
* Fix: support tags__id__none in advanced search, fix tags filter badge count for excluded @shamoon (#2205)
* Bugfix: Don't run system checks on migrate @stumpylog (#2183)
* Bugfix: Decoding task signals could fail on datetime type @stumpylog (#2058)

[1.5.0]
* Install nltk data

[1.5.1]
* Update Paperless-ngx to 1.11.3
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.11.3)
* Bugfix: Return created task ID when posting document to API @stumpylog (#2279)
* Bugfix: Fix no content when processing some RTL files @stumpylog (#2295)
* Bugfix: Handle email dates maybe being naive @stumpylog (#2293)
* Fix: live filterable dropdowns broken in 1.11.x @shamoon (#2292)
* Bugfix: Reading environment from files didn't work for management commands @stumpylog (#2261)
* Bugfix: Return created task ID when posting document to API @stumpylog (#2279)

[1.5.2]
* Update Paperless-ngx to 1.12.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.12.1)
* Fix: comments not showing in search until after manual reindex in v1.12 @shamoon (#2513)
* Fix: date range search broken in 1.12 @shamoon (#2509)

[1.5.3]
* Update Paperless-ngx to 1.12.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.12.2)
* Bugfix: Allow pre-consume scripts to modify incoming file @stumpylog (#2547)
* Bugfix: Return to page based barcode scanning @stumpylog (#2544)
* Fix: Try to prevent title debounce overwriting @shamoon (#2543)
* Fix comment search highlight + multi-word search @shamoon (#2542)
* Bugfix: Request PDF/A format from Gotenberg @stumpylog (#2530)
* Fix: Trigger reindex for pre-existing comments @shamoon (#2519)

[1.5.4]
* Fix NLTK data pth

[1.6.0]
* Update Paperless-ngx to 1.13.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.13.0)
* Feature: allow disable warn on close saved view with changes @shamoon (#2681)
* Feature: Add option to enable response compression @stumpylog (#2621)
* Feature: split documents on ASN barcode @muued (#2554)
* Fix: Ignore path filtering didn't handle sub directories @stumpylog (#2674)
* Bugfix: Generation of secret key hangs during install script @stumpylog (#2657)
* Fix: Remove files produced by barcode splitting when completed @stumpylog (#2648)
* Fix: add missing storage path placeholders @shamoon (#2651)
* Fix long dropdown contents break document detail column view @shamoon (#2638)
* Fix: tags dropdown should stay closed when removing @shamoon (#2625)
* Bugfix: Configure scheduled tasks to expire after some time @stumpylog (#2614)
* Bugfix: Limit management list pagination maxSize to 5 @Kaaybi (#2618)
* Fix: Don't crash on bad ASNs during indexing @stumpylog (#2586)
* Fix: Prevent mktime OverflowError except in even more rare caes @stumpylog (#2574)
* Bugfix: Whoosh relative date queries weren't handling timezones @stumpylog (#2566)
* Fix importing files with non-ascii names @Kexogg (#2555)

[1.7.0]
* Update Paperless-ngx to 1.14.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.14.0)
* Feature: multi-user permissions @shamoon (#2147)
* Feature: Stronger typing for file consumption @stumpylog (#2744)
* Feature: double-click docs @shamoon (#2966)
* feature: Add support for zxing as barcode scanning lib @margau (#2907)
* Feature: Enable images to be released on Quay.io @stumpylog (#2972)
* Feature: test mail account @shamoon (#2949)
* Feature: Capture celery and kombu logs to a file @stumpylog (#2954)
* Fix: Resolve Redis connection issues with ACLs @stumpylog (#2939)
* Feature: Allow mail account to use access tokens @stumpylog (#2930)
* Fix: Consumer polling could overwhelm database @stumpylog (#2922)
* Feature: Improved statistics widget @shamoon (#2910)
* Enhancement: rename comments to notes and improve notes UI @shamoon (#2904)
* Allow psql client certificate authentication @Ongy (#2899)
* Enhancement: support filtering multiple correspondents, doctypes & storage paths @shamoon (#2893)

[1.7.1]
* Update Paperless-ngx to 1.14.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.14.1)
* Fix: reduce frequency of permissions queries to speed up v1.14.0 @shamoon (#3201)
* Fix: permissions-aware statistics @shamoon (#3199)
* Fix: Use document owner for matching if set @shamoon (#3198)
* Fix: respect permissions on document view actions @shamoon (#3174)
* Increment API version for 1.14.1+ @shamoon (#3191)
* Fix: dropdown Private items with empty set @shamoon (#3189)
* Documentation: add gnu-sed note for macOS @shamoon (#3190)

[1.7.2]
* Update Paperless-ngx to 1.14.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.14.2)
* Feature: Finnish translation @shamoon (#3215)
* Fix: Load saved views from app frame, not dashboard @shamoon (#3211)
* Fix: advanced search or date searching + doc type/correspondent/storage path broken @shamoon (#3209)
* Fix MixedContentTypeError in `add_inbox_tags` handler @e1mo (#3212)

[1.7.3]
* Update Paperless-ngx to 1.14.3
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.14.3)
* Enhancement: better keyboard nav for filter/edit dropdowns @shamoon (#3227)
* Bump filelock from 3.10.2 to 3.12.0 to fix permissions bug @rbrownwsws (#3282)
* Fix: Handle cases where media files aren't all in the same filesystem @stumpylog (#3261)
* Fix: Prevent erroneous warning when starting container @stumpylog (#3262)
* Retain doc changes on tab switch after refresh doc @shamoon (#3243)
* Fix: Don't send Gmail related setting if the server doesn't support it @stumpylog (#3240)
* Fix: close all docs on logout @shamoon (#3232)
* Fix: Respect superuser for advanced queries, test coverage for object perms @shamoon (#3222)
* Fix: ALLOWED_HOSTS logic being overwritten when * is set @ikaruswill (#3218)

[1.7.4]
* Update Paperless-ngx to 1.14.4
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.14.4)
* Fix: Inversion in tagged mail searching @stumpylog (#3305)
* Fix dynamic count labels hidden in light mode @shamoon (#3303)

[1.7.5]
* Update Paperless-ngx to 1.14.5
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.14.5)
* Fix: respect permissions for autocomplete suggestions @shamoon (#3359)
* Feature: owner filtering @shamoon (#3309)
* Enhancement: dynamic counts include all pages, hide for "Any" @shamoon (#3329)
* Chore: Upgrades Python dependencies to their latest allowed versions @stumpylog (#3365)
* Enhancement: save tour completion, hide welcome widget @shamoon (#3321)
* Fix: Adds better handling for files with invalid utf8 content @stumpylog (#3387)
* Fix: respect permissions for autocomplete suggestions @shamoon (#3359)
* Fix: Transition to new library for finding IPs for failed logins @stumpylog (#3382)
* [Security] Render frontend text as plain text @shamoon (#3366)
* Fix: default frontend to current owner, allow setting no owner on create @shamoon (#3347)
* Fix: dont perform mail actions when rule filename filter not met @shamoon (#3336)
* Fix: permission-aware bulk editing in 1.14.1+ @shamoon (#3345)

[1.8.0]
* Update Paperless-ngx to 1.15.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.15.0)
* Feature: quick filters from document detail @shamoon (#3476)
* Feature: Add explanations to relative dates @shamoon (#3471)
* Enhancement: paginate frontend tasks @shamoon (#3445)
* Feature: Better encapsulation of barcode logic @stumpylog (#3425)
* Enhancement: Improve frontend error handling @shamoon (#3413)

[1.9.0]
* Update Paperless-ngx to 1.16.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.16.0)
* Feature: Update to a simpler Tika library @stumpylog (#3517)
* Feature: Allow to filter documents by original filename and checksum @jayme-github (#3485)
* Fix: return user first / last name from backend @shamoon (#3579)
* Fix use of `PAPERLESS_DB_TIMEOUT` for all db types @shamoon (#3576)
* Fix: handle mail rules with no filters on some imap servers @shamoon (#3554)

[1.9.1]
* Update Paperless-ngx to 1.16.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.16.1)
* Chore: Enable the image cleanup action @stumpylog (#3606)

[1.9.2]
* Update Paperless-ngx to 1.16.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.16.2)
* Fix: Increase httpx operation timeouts to 30s @stumpylog (#3627)
* Fix: Better error handling and checking when parsing documents via Tika @stumpylog (#3617)

[1.9.3]
* Update Paperless-ngx to 1.16.3
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.16.3)
* Fix: Set user and home environment through supervisord @stumpylog (#3638)
* Fix: Ignore errors when trying to copy the original file's stats @stumpylog (#3652)
* Copy default thumbnail if thumbnail generation fails @plu (#3632)
* Fix: Set user and home environment through supervisord @stumpylog (#3638)
* Fix: Fix quick install with external database not being fully ready @stumpylog (#3637)

[1.9.4]
* Update Paperless-ngx to 1.16.4
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.16.4)
* Fix: prevent button wrapping when sidebar narrows in MS Edge @shamoon (#3682)
* Fix: Handling for filenames with non-ascii and no content attribute @stumpylog (#3695)
* Fix: Generation of thumbnails for existing stored emails @stumpylog (#3696)
* Fix: Use row gap for filter editor @kleinweby (#3662)

[1.9.5]
* Update Paperless-ngx to 1.16.5
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.16.5)
* Feature: support barcode upscaling for better detection of small barcodes @bmachek (#3655)
* Fix: owner removed when `set_permissions` passed on object create @shamoon (#3702)

[1.10.0]
* Update Paperless-ngx to 1.17.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.17.0)
* Add support for additional UK date formats @brainrecursion (#3887)
* Add 'doc_pk' to PAPERLESS_FILENAME_FORMAT handling @mechanarchy (#3861)
* Feature: hover buttons for saved view widgets @shamoon (#3875)
* Feature: collate two single-sided multipage scans @brakhane (#3784)
* Feature: include global and object-level permissions in export / import @shamoon (#3672)
* Enhancement / Fix: Migrate encrypted png thumbnails to webp @shamoon (#3719)
* Feature: Add Slovak translation @shamoon (#3722)
* Fix: cancel possibly slow queries on doc details @shamoon (#3925)
* Fix: note creation / deletion should respect doc permissions @shamoon (#3903)
* Fix: notes show persistent scrollbars @shamoon (#3904)
* Fix: Provide SSL context to IMAP client @stumpylog (#3886)
* Fix/enhancement: permissions for mail rules & accounts @shamoon (#3869)
* Fix: Classifier special case when no items are set to automatic matching @stumpylog (#3858)
* Fix: issues with copy2 or copystat and SELinux permissions @stumpylog (#3847)
* Fix: Parsing office document timestamps @stumpylog (#3836)
* Fix: Add warning to install script need for permissions @shamoon (#3835)
* Fix interaction between API and barcode archive serial number @stumpylog (#3834)
* Enhancement / Fix: Migrate encrypted png thumbnails to webp @shamoon (#3719)
* Fix: add UI tour step padding @hakimio (#3791)
* Fix: translate file tasks types in footer @shamoon (#3749)
* Fix: limit ng-select size for addition of filter button @shamoon (#3731)

[1.10.1]
* Update Paperless-ngx to 1.17.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.17.1)
* Fix / Enhancement: restrict status messages by owner if set & improve 404 page @shamoon (#3959)
* Feature: Add Ukrainian translation @shamoon (#3941)

[1.10.2]
* Update Paperless-ngx to 1.17.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.17.2)
* Enhancement: Allow to set a prefix for keys and channels in redis @amo13 (#3993)
* Fix: Increase the HTTP timeouts for Tika/Gotenberg to maximum task time @stumpylog (#4061)
* Fix: Allow adding an SSL certificate for IMAP SSL context @stumpylog (#4048)
* Fix: tag creation sometimes retained search text @shamoon (#4038)
* Fix: enforce permissions on `bulk_edit` operations @shamoon (#4007)

[1.10.3]
* Update Paperless-ngx to 1.17.3
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.17.3)
* Fix: When PDF/A rendering fails, add a consideration for the user to add args to override @stumpylog (#4083)

[1.10.4]
* Update Paperless-ngx to 1.17.4
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v1.17.4)
* Fix: ghostscript rendering error doesnt trigger frontend failure message @shamoon (#4092)

[1.11.0]
* Update base image to 4.2.0

[1.12.0]
* Update Paperless-ngx to 2.0.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.0.0)

[1.12.1]
* Update Paperless-ngx to 2.0.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.0.1)
* Fix: Increase field the length for consumption template source @stumpylog (#4719)
* Fix: Set RGB color conversion strategy for PDF outputs @stumpylog (#4709)
* Fix: Add a warning about a low image DPI which may cause OCR to fail @stumpylog (#4708)
* Fix: share links for URLs containing 'api' incorrect in dropdown @shamoon (#4701)

[1.13.0]
* Update Paperless-ngx to 2.1.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.1.0)
* Enhancement: implement document link custom field @shamoon (#4799)
* Feature: Adds additional warnings during an import if it might fail @stumpylog (#4814)
* Feature: pngx PDF viewer with updated pdfjs @shamoon (#4679)
* Enhancement: support automatically assigning custom fields via consumption templates @shamoon (#4727)
* Feature: update user profile @shamoon (#4678)
* Enhancement: Allow excluding mail attachments by name @stumpylog (#4691)
* Enhancement: auto-refresh logs & tasks @shamoon (#4680)

[1.13.1]
* Update Paperless-ngx to 2.1.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.1.1)
* Fix: disable toggle for share link creation without archive version, fix auto-copy in Safari @shamoon (#4885)
* Fix: storage paths link incorrect in dashboard widget @shamoon (#4878)
* Fix: respect baseURI for pdfjs worker URL @shamoon (#4865)
* Fix: Allow users to configure the "From" email for password reset @stumpylog (#4867)
* Fix: dont show move icon for file tasks badge @shamoon (#4860)

[1.13.2]
* Update Paperless-ngx to 2.1.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.1.2)
* Fix: allow text copy in pngx pdf viewer @shamoon (#4938)
* Fix: sort consumption templates by order by default @shamoon (#4956)
* Fix: Updates gotenberg-client, including workaround for Gotenberg non-latin handling @stumpylog (#4944)
* Fix: allow text copy in pngx pdf viewer @shamoon (#4938)
* Fix: Don't allow autocomplete searches to fail on schema field matches @stumpylog (#4934)
* Fix: Convert search dates to UTC in advanced search @bogdal (#4891)
* Fix: Use the attachment filename so downstream template matching works @stumpylog (#4931)
* Fix: frontend handle autocomplete failure gracefully @shamoon (#4903)

[1.13.3]
* Update Paperless-ngx to 2.1.3
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.1.3)
* Fix: Document metadata is lost during barcode splitting @stumpylog ([#4982](https://github.com/paperless-ngx/paperless-ngx/pull/4982))
* Fix: Export of custom field instances during a split manifest export @stumpylog ([#4984](https://github.com/paperless-ngx/paperless-ngx/pull/4984))
* Fix: Apply user arguments even in the case of the forcing OCR @stumpylog ([#4981](https://github.com/paperless-ngx/paperless-ngx/pull/4981))
* Fix: support show errors for select dropdowns @shamoon ([#4979](https://github.com/paperless-ngx/paperless-ngx/pull/4979))
* Fix: Don't attempt to parse none objects during date searching @bogdal ([#4977](https://github.com/paperless-ngx/paperless-ngx/pull/4977))
* Refactor: Boost performance by reducing db queries @bogdal ([#4990](https://github.com/paperless-ngx/paperless-ngx/pull/4990))
* Fix: Document metadata is lost during barcode splitting @stumpylog ([#4982](https://github.com/paperless-ngx/paperless-ngx/pull/4982))
* Fix: Export of custom field instances during a split manifest export @stumpylog ([#4984](https://github.com/paperless-ngx/paperless-ngx/pull/4984))
* Fix: Apply user arguments even in the case of the forcing OCR @stumpylog ([#4981](https://github.com/paperless-ngx/paperless-ngx/pull/4981))
* Fix: support show errors for select dropdowns @shamoon ([#4979](https://github.com/paperless-ngx/paperless-ngx/pull/4979))
* Fix: Don't attempt to parse none objects during date searching @bogdal ([#4977](https://github.com/paperless-ngx/paperless-ngx/pull/4977))

[1.14.0]
* Update Paperless-ngx to 2.2.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.2.0)
* Enhancement: Add tooltip for select dropdown items @shamoon (#5070)
* Chore: Update Angular to v17 including new Angular control-flow @shamoon (#4980)
* Enhancement: symmetric document links @shamoon (#4907)
* Enhancement: shared icon & shared by me filter @shamoon (#4859)
* Enhancement: Improved popup preview, respect embedded viewer, error handling @shamoon (#4947)
* Enhancement: Allow deletion of documents via the fuzzy matching command @stumpylog (#4957)
* Enhancement: document link field fixes @shamoon (#5020)
* Enhancement: above and below doc detail save buttons @shamoon (#5008)
* Fix: Case where a mail attachment has no filename to use @stumpylog (#5117)
* Fix: Disable auto-login for API token requests @shamoon (#5094)
* Fix: update ASN regex to support Unicode @eukub (#5099)
* Fix: ensure CSRF-Token on Index view @baflo (#5082)
* Fix: Stop auto-refresh logs / tasks after close @shamoon (#5089)
* Fix: Make the admin panel accessible when using a large number of documents @bogdal (#5052)
* Fix: dont allow null custom_fields property via API @shamoon (#5063)
* Fix: Updates Ghostscript to 10.02.1 for more bug fixes to it @stumpylog (#5040)
* Fix: allow system keyboard shortcuts in date fields @shamoon (#5009)
* Fix password change detection on profile edit @shamoon (#5028)

[1.14.1]
* Update Paperless-ngx to 2.2.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.2.1)
* Fix: saving doc links with no value @shamoon (#5144)
* Fix: allow multiple consumption templates to assign the same custom field @shamoon (#5142)
* Fix: some dropdowns broken in 2.2.0 @shamoon (#5134)

[1.15.0]
* Update Paperless-ngx to 2.3.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.3.0)
* Feature: Workflows @shamoon (#5121)
* Feature: Allow setting backend configuration settings via the UI @stumpylog (#5126)
* Feature: Workflows @shamoon (#5121)
* Feature: Allow setting backend configuration settings via the UI @stumpylog (#5126)
* Enhancement: fetch mails in bulk @falkenbt (#5249)
* Enhancement: add storage_path parameter to post_document API @bevanjkay (#5217)

[1.15.1]
* Update Paperless-ngx to 2.3.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.3.1)
* Fix: edit workflow form not displaying trigger settings @shamoon (#5276)
* Fix: Prevent passing 0 pages to OCRMyPDF @stumpylog (#5275)

[1.15.2]
* Update Paperless-ngx to 2.3.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.3.2)
* Fix: triggered workflow assignment of customfield fails if field exists in v2.3.1 @shamoon (#5302)
* Fix: Decoding of user arguments for OCR @stumpylog (#5307)
* Fix: empty workflow trigger match field cannot be saved in v.2.3.1 @shamoon (#5301)
* Fix: Use local time for added/updated workflow triggers @stumpylog (#5304)
* Fix: workflow edit form loses unsaved changes @shamoon (#5299)

[1.15.3]
* Update Paperless-ngx to 2.3.3
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.3.3)
* Enhancement: Explain behavior of unset app config boolean to user @shamoon (#5345)
* Enhancement: title assignment placeholder error handling, fallback @shamoon (#5282)
* Fix: Don't require the JSON user arguments field, interpret empty string as null @stumpylog (#5320)
* Chore: Backend dependencies update @stumpylog (#5336)
* Chore: add pre-commit hook for codespell @shamoon (#5324)

[1.16.0]
* Update Paperless-ngx to 2.4.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.4.0)
* Enhancement: support remote user auth directly against API (DRF) @shamoon (#5386)
* Feature: Add additional caching support to suggestions and metadata @stumpylog (#5414)
* Feature: help tooltips @shamoon (#5383)
* Enhancement: warn when outdated doc detected @shamoon (#5372)
* Feature: app branding @shamoon (#5357)
* Fix: doc link removal when has never been assigned @shamoon (#5451)
* Fix: dont lose permissions ui if owner changed from null @shamoon (#5433)
* Fix: Getting next ASN when no documents have an ASN @stumpylog (#5431)
* Fix: signin username floating label @shamoon (#5424)
* Fix: "shared by me" filter with multiple users / groups in postgres @shamoon (#5396)
* Fix: Catch new warning when loading the classifier @stumpylog (#5395)
* Fix: doc detail component fixes @shamoon (#5373)

[1.16.1]
* Update Paperless-ngx to 2.4.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.4.1)
* Fix: Minor frontend things in 2.4.0 @shamoon (#5514)
* Fix: enforce permissions for app config @shamoon (#5516)
* Fix: render images not converted to pdf, refactor doc detail rendering @shamoon (#5475)
* Fix: Dont parse numbers with exponent as integer @shamoon (#5457)

[1.16.2]
* Update Paperless-ngx to 2.4.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.4.2)
* Fix: improve one of the date matching regexes @shamoon (#5540)
* Fix: tweak doc detail component behavior while awaiting metadata @shamoon (#5546)

[1.16.3]
* Update Paperless-ngx to 2.4.3
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.4.3)
* Fix: Ensure the scratch directory exists before consuming via the folder @stumpylog (#5579)

[1.17.0]
* Pre-configure sendmail
* Remove recvmail addon. Paperless supports multiple imap sources and should be configured in
  the app directly.

[1.18.0]
* Update Paperless-ngx to 2.5.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.5.0)
* Enhancement: bulk delete objects @shamoon (#5688)
* Enhancement: confirm buttons @shamoon (#5680)
* Enhancement: bulk delete objects @shamoon (#5688)
* Feature: allow create objects from bulk edit @shamoon (#5667)
* Feature: Allow tagging by putting barcodes on documents @pkrahmer (#5580)
* Feature: Cache metadata and suggestions in Redis @stumpylog (#5638)
* Feature: Japanese translation @shamoon (#5641)
* Feature: option for auto-remove inbox tags on save @shamoon (#5562)
* Enhancement: allow paperless to run in read-only filesystem @hegerdes (#5596)
* Enhancement: mergeable bulk edit permissions @shamoon (#5508)
* Enhancement: re-implement remote user auth for unsafe API requests as opt-in @shamoon (#5561)
* Enhancement: Respect PDF cropbox for thumbnail generation @henningBunk (#5531)
* Fix: Test metadata items for Unicode issues @stumpylog (#5707)
* Change: try to show preview even if metadata fails @shamoon (#5706)
* Fix: only check workflow trigger source if not empty @shamoon (#5701)
* Fix: frontend validation of number fields fails upon save @shamoon (#5646)
* Fix: Explicit validation of custom field name unique constraint @shamoon (#5647)
* Fix: Don't attempt to retrieve object types user doesnt have permissions to @shamoon (#5612)

[1.18.1]
* Update Paperless-ngx to 2.5.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.5.1)
* Fix: Splitting on ASN barcodes even if not enabled @stumpylog (#5740)
* Chore(deps-dev): Bump the development group with 2 updates @dependabot (#5737)
* Chore(deps): Bump the django group with 1 update @dependabot (#5739)

[1.18.2]
* Update Paperless-ngx to 2.5.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.5.2)
* Fix: Generated secret key may include single or double quotes @schmidtnz (#5767)
* Fix: consumer status alerts container blocks elements @shamoon (#5762)
* Fix: handle document notes user format api change @shamoon (#5751)
* Fix: Assign ASN from barcode only after any splitting @stumpylog (#5745)

[1.18.3]
* Update Paperless-ngx to 2.5.3
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.5.3)
* Fix: dont allow allauth redirects to any host @shamoon (#5783)
* Fix: Interaction when both splitting and ASN are enabled @stumpylog (#5779)
* Fix: moved ssl_mode parameter for mysql backend engine @MaciejSzczurek (#5771)

[1.19.0]
* Add OIDC login

[1.19.1]
* Update Paperless-ngx to 2.5.4
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.5.4)
* Fix: handle title placeholder for docs without original_filename @shamoon (#5828)
* Fix: bulk edit objects does not respect global permissions @shamoon (#5888)
* Fix: intermittent save & close warnings @shamoon (#5838)
* Fix: inotify read timeout not in ms @grembo (#5876)
* Fix: allow relative date queries not in quick list @shamoon (#5801)
* Fix: pass rule id to consumed .eml files @shamoon (#5800)

[1.20.0]
* Update Paperless-ngx to 2.6.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.6.0)
* Enhancement: Allow user to control PIL image pixel limit @stumpylog (#5997)
* Enhancement: Allow a user to disable the pixel limit for OCR entirely @stumpylog (#5996)
* Feature: workflow removal action @shamoon (#5928)
* Feature: system status @shamoon (#5743)
* Enhancement: better monetary field with currency code @shamoon (#5858)
* Feature: support disabling regular login @shamoon (#5816)

[1.20.1]
* Update Paperless-ngx to 2.6.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.6.1)
* Change: tweaks to system status @shamoon (#6008)

[1.20.2]
* Update Paperless-ngx to 2.6.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.6.2)
* Enhancement: move and rename files when storage paths deleted, update file handling docs @shamoon (#6033)
* Fix: make document counts in object lists permissions-aware @shamoon (#6019)

[1.20.3]
* Update Paperless-ngx to 2.6.3
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.6.3)
* Fix: allow setting allauth `ACCOUNT_SESSION_REMEMBER` @shamoon (#6105)
* Change: dont require empty bulk edit parameters @shamoon (#6059)

[1.21.0]
* Update Paperless-ngx to 2.7.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.7.0)
* Feature: PDF actions - merge, split & rotate @shamoon (#6094)
* Change: enable auditlog by default, fix import / export @shamoon (#6267)
* Enhancement: always place search term first in autocomplete results @shamoon (#6142)
* Fix: Escape the secret key when writing it to the env file @stumpylog (#6243)
* Fix: Hide sidebar labels if group is empty @shamoon (#6254)
* Fix: management list clear all should clear header checkbox @shamoon (#6253)
* Fix: start-align object names in some UI lists @shamoon (#6188)
* Fix: allow scroll long upload files alerts list @shamoon (#6184)
* Fix: `document_renamer` fails with `audit_log` enabled @shamoon (#6175)
* Fix: catch sessionStorage errors for large documents @shamoon (#6150)

[1.21.1]
* Update Paperless-ngx to 2.7.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.7.1)
* Fix: Only disable split button if pages = 1 @shamoon (#6304)
* Fix: Use correct custom field id when splitting @shamoon (#6303)
* Fix: Rotation fails due to celery chord @stumpylog (#6306)
* Fix: split user / group objects error @shamoon (#6302)

[1.21.2]
* Update Paperless-ngx to 2.7.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.7.2)
* Fix: select dropdown background colors not visible in light mode @shamoon (#6323)
* Fix: spacing in reset and incorrect display in saved views @shamoon (#6324)
* Fix: disable invalid create endpoints @shamoon (#6320)
* Fix: dont initialize page numbers, allow split with browser pdf viewer @shamoon (#6314)
* Miscellaneous other fixes & improvements

[1.22.0]
* Update Paperless-ngx to 2.8.1
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.8.1)
* Fix: remove admin.logentry perm, use admin (staff) status @shamoon (#6380)
* Feature: global search, keyboard shortcuts / hotkey support @shamoon (#6449)
* Feature: custom fields filtering & bulk editing @shamoon (#6484)
* Feature: customizable fields display for documents, saved views & dashboard widgets @shamoon (#6439)
* Feature: document history (audit log UI) @shamoon (#6388)
* Chore: Convert the consumer to a plugin @stumpylog (#6361)
* Chore(deps): Bump all allowed backend packages @stumpylog (#6562)
* Feature: global search, keyboard shortcuts / hotkey support @shamoon (#6449)
* Feature: customizable fields display for documents, saved views & dashboard widgets @shamoon (#6439)
* Feature: document history (audit log UI) @shamoon (#6388)
* Enhancement: refactor monetary field @shamoon (#6370)
* Chore: Convert the consumer to a plugin @stumpylog (#6361)
* Fix: always check workflow filter_mailrule if set @shamoon (#6474)
* Fix: use responsive tables for management lists @DlieBG (#6460)
* Fix: password reset done template @shamoon (#6444)
* Fix: show message on empty group list @DlieBG (#6393)
* Fix: remove admin.logentry perm, use admin (staff) status @shamoon (#6380)
* Fix: dont dismiss active alerts on "dismiss completed" @shamoon (#6364)
* Fix: Allow lowercase letters in monetary currency code field @shamoon (#6359)
* Fix: Allow negative monetary values with a current code @stumpylog (#6358)
* Fix: bulk edit custom fields should support multiple items @shamoon (#6589)
* Fix: bulk edit custom fields should support multiple items @shamoon (#6589)

[1.22.1]
* Update Paperless-ngx to 2.8.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.8.2)
* Fix: Restore the compression of static files for x86_64 @stumpylog (#6627)
* Fix: make backend monetary validation accept unpadded decimals @shamoon (#6626)
* Fix: allow bulk edit with existing fields @shamoon (#6625)
* Fix: table view doesnt immediately display custom fields on app startup @shamoon (#6600)
* Fix: dont use limit in subqueries in global search for mariadb compatibility @shamoon (#6611)
* Fix: exclude admin perms from group permissions serializer @shamoon (#6608)
* Fix: global search text illegible in light mode @shamoon (#6602)
* Fix: document history text color illegible in light mode @shamoon (#6601)

[1.22.2]
* Update Paperless-ngx to 2.8.3
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.8.3)
* Fix: respect superuser for document history @shamoon (#6661)
* Fix: allow 0 in monetary field @shamoon (#6658)
* Fix: custom field removal doesnt always trigger change detection @shamoon (#6653)
* Fix: Downgrade and lock lxml @stumpylog (#6655)
* Fix: correctly handle global search esc key when open and button foucsed @shamoon (#6644)
* Fix: consistent monetary field display in list and cards @shamoon (#6645)
* Fix: doc links and more illegible in light mode @shamoon (#6643)
* Fix: Allow auditlog to be disabled @stumpylog (#6638)

[1.22.3]
* Update Paperless-ngx to 2.8.4
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.8.4)
* Enhancement: display current ASN in statistics @darmiel (#6692)
* Enhancement: global search tweaks @shamoon (#6674)
* Security: Correctly disable eval in pdfjs @shamoon (#6702)
* Fix: history timestamp tooltip illegible in dark mode @shamoon (#6696)
* Fix: only count inbox documents from inbox tags with permissions @shamoon (#6670)

[1.22.4]
* Update Paperless-ngx to 2.8.5
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.8.5)
* Fix: restore search highlighting on large cards results @shamoon (#6728)
* Fix: global search filtering links broken in 2.8.4 @shamoon (#6726)
* Fix: some buttons incorrectly aligned in 2.8.4 @shamoon (#6715)
* Fix: don't format ASN as number on dashboard @shamoon (#6708)

[1.22.5]
* Update Paperless-ngx to 2.8.6
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.8.6)
* Security: disallow API remote-user auth if disabled @shamoon (#6739)
* Fix: retain sort field from global search filtering, use `FILTER_HAS_TAGS_ALL` @shamoon (#6737)

[1.23.0]
* Update Paperless-ngx to 2.9.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.9.0)
* Feature: Allow a data only export/import cycle @stumpylog (#6871)
* Change: rename 'redo OCR' to 'reprocess' to clarify behavior @shamoon (#6866)
* Enhancement: Support custom path for the classification file @lino-b (#6858)
* Enhancement: default to title/content search, allow choosing full search link from global search @shamoon (#6805)
* Enhancement: only include correspondent 'last_correspondence' if requested @shamoon (#6792)
* Enhancement: delete pages PDF action @shamoon (#6772)
* Enhancement: support custom logo / title on login page @shamoon (#6775)

[1.24.0]
* Update Paperless-ngx to 2.10.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.10.2)
* Feature: documents trash aka soft delete @shamoon (#6944)
* Enhancement: better boolean custom field display @shamoon (#7001)
* Feature: Allow encrypting sensitive fields in export @stumpylog (#6927)
* Enhancement: allow consumption of odg files @daniel-boehme (#6940)

[1.25.0]
* Update Paperless-ngx to 2.11.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.11.0)
* Enhancement: disable add split button when approrpriate @shamoon (#7215)
* Enhancement: wrapping of saved view fields d-n-d UI @shamoon (#7216)
* Enhancement: support custom field icontains filter for select type @shamoon (#7199)
* Feature: select custom field type @shamoon (#7167)
* Feature: automatic sso redirect @shamoon (#7168)
* Enhancement: show more columns in mail frontend admin @shamoon (#7158)
* Enhancement: use request user as owner of split / merge docs @shamoon (#7112)
* Enhancement: improve date parsing with accented characters @fdubuy (#7100)
* Feature: improve history display of object names etc @shamoon (#7102)

[1.25.1]
* Update Paperless-ngx to 2.11.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.11.2)
* Change: more clearly handle init permissions error @shamoon (#7334)
* Chore: add permissions info link from webUI @shamoon (#7310)

[1.25.2]
* Update Paperless-ngx to 2.11.3
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.11.3)
* Enhancement: optimize tasks / stats reload @shamoon (#7402)
* Enhancement: allow specifying default currency for Monetary custom field @shamoon (#7381)
* Enhancement: specify when pre-check fails for documents in trash @shamoon (#7355)
* Fix: clear selection after reload for management lists @shamoon (#7421)
* Fix: disable inline create buttons if insufficient permissions @shamoon (#7401)
* Fix: use entire document for dropzone @shamoon (#7342)

[1.25.3]
* Update Paperless-ngx to 2.11.4
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.11.4)
* Fix: initial upload message not being dismissed @shamoon (#7438)

[1.25.4]
* Update Paperless-ngx to 2.11.6
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.11.6)
* Fix: fix nltk tokenizer breaking change @shamoon (#7522)
* Fix: use JSON for update archive file auditlog entries @shamoon (#7503)
* Fix: respect deskew / rotate pages from AppConfig if set @shamoon (#7501)

[1.25.5]
* Fix nltk issue using `punkt_tab` instead of `punkt`

[1.26.0]
* Update Paperless-ngx to 2.12.0
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.12.0)
* Enhancement: re-work mail rule dialog, support multiple include patterns @shamoon (#7635)
* Enhancement: add Korean language @shamoon (#7573)
* Enhancement: allow multiple filename attachment exclusion patterns for a mail rule @MelleD (#5524)
* Refactor: Use django-filter logic for filtering full text search queries @yichi-yang (#7507)
* Refactor: Reduce number of SQL queries when serializing List[Document] @yichi-yang (#7505)

[1.27.0]
* Add tika and gotenberg

[1.27.1]
* Update Paperless-ngx to 2.13.4
* Fix: fix dark mode icon blend mode in 2.13.3
* Fix: fix clipped popup preview in 2.13.3
* Fix: fix dark mode icon blend mode in 2.13.3
* Fix: fix clipped popup preview in 2.13.3

[1.28.0]
* Update Paperless-ngx to 2.13.2
* [Full changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.13.2)

[1.29.0]
* Update gotenberg to 8.13.0

[1.29.1]
* Fix gotenberg
* Update paperless-ngx to 2.13.5
* [Full Changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.13.2)
* Fix: handle page count exception for pw-protected files [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8240](https://github.com/paperless-ngx/paperless-ngx/pull/8240))
* Fix: correctly track task id in list for change detection [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8230](https://github.com/paperless-ngx/paperless-ngx/pull/8230))
* Fix: Admin pages should show trashed documents [@&#8203;stumpylog](https://github.com/stumpylog) ([#&#8203;8068](https://github.com/paperless-ngx/paperless-ngx/pull/8068))
* Fix: tag colors shouldn't change when selected in list [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8225](https://github.com/paperless-ngx/paperless-ngx/pull/8225))
* Fix: fix re-activation of save button when changing array items [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8208](https://github.com/paperless-ngx/paperless-ngx/pull/8208))
* Fix: fix thumbnail clipping, select inverted color in safari dark mode not system [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8193](https://github.com/paperless-ngx/paperless-ngx/pull/8193))
* Fix: select checkbox should remain visible [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8185](https://github.com/paperless-ngx/paperless-ngx/pull/8185))
* Fix: warn with proper error on ASN exists in trash [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8176](https://github.com/paperless-ngx/paperless-ngx/pull/8176))
* Chore: Updates all runner images to use Ubuntu Noble [@&#8203;stumpylog](https://github.com/stumpylog) ([#&#8203;8213](https://github.com/paperless-ngx/paperless-ngx/pull/8213))
* Chore(deps): Bump stumpylog/image-cleaner-action from 0.8.0 to 0.9.0 in the actions group [@&#8203;dependabot](https://github.com/dependabot) ([#&#8203;8142](https://github.com/paperless-ngx/paperless-ngx/pull/8142))
* Chore(deps): Bump stumpylog/image-cleaner-action from 0.8.0 to 0.9.0 in the actions group [@&#8203;dependabot](https://github.com/dependabot) ([#&#8203;8142](https://github.com/paperless-ngx/paperless-ngx/pull/8142))
* Fix: handle page count exception for pw-protected files [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8240](https://github.com/paperless-ngx/paperless-ngx/pull/8240))
* Fix: correctly track task id in list for change detection [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8230](https://github.com/paperless-ngx/paperless-ngx/pull/8230))
* Fix: Admin pages should show trashed documents [@&#8203;stumpylog](https://github.com/stumpylog) ([#&#8203;8068](https://github.com/paperless-ngx/paperless-ngx/pull/8068))
* Fix: tag colors shouldn't change when selected in list [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8225](https://github.com/paperless-ngx/paperless-ngx/pull/8225))
* Fix: fix re-activation of save button when changing array items [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8208](https://github.com/paperless-ngx/paperless-ngx/pull/8208))
* Fix: fix thumbnail clipping, select inverted color in safari dark mode not system [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8193](https://github.com/paperless-ngx/paperless-ngx/pull/8193))
* Fix: select checkbox should remain visible [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8185](https://github.com/paperless-ngx/paperless-ngx/pull/8185))
* Fix: warn with proper error on ASN exists in trash [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8176](https://github.com/paperless-ngx/paperless-ngx/pull/8176))


[1.30.0]
* Update gotenberg to 8.14.0

[1.30.1]
* Update gotenberg to 8.14.1

[1.30.2]
* CLOUDRON_OIDC_PROVIDER_NAME implemented

[1.30.3]
* Update gotenberg to 8.15.0

[1.30.4]
* Update gotenberg to 8.15.1

[1.30.5]
* Update gotenberg to 8.15.2

[1.30.6]
* Update gotenberg to 8.15.3
* [Full Changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.13.2)
* Fix: remove auth tokens from export @shamoon (#8100)
* Fix: cf query dropdown styling affecting other components @shamoon (#8095)

[1.31.0]
* Update paperless-ngx to 2.14.1
* [Full Changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.13.2)
* Fix: prevent error if bulk edit method not in MODIFIED_FIELD_BY_METHOD [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8710](https://github.com/paperless-ngx/paperless-ngx/pull/8710))
* Fix: include tag component in list view [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8706](https://github.com/paperless-ngx/paperless-ngx/pull/8706))
* Fix: use unmodified original for checksum if exists [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8693](https://github.com/paperless-ngx/paperless-ngx/pull/8693))
* Fix: complete load with native PDF viewer [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8699](https://github.com/paperless-ngx/paperless-ngx/pull/8699))
* Fix: prevent error if bulk edit method not in MODIFIED_FIELD_BY_METHOD [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8710](https://github.com/paperless-ngx/paperless-ngx/pull/8710))
* Fix: include tag component in list view [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8706](https://github.com/paperless-ngx/paperless-ngx/pull/8706))
* Fix: use unmodified original for checksum if exists [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8693](https://github.com/paperless-ngx/paperless-ngx/pull/8693))
* Fix: complete load with native PDF viewer [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8699](https://github.com/paperless-ngx/paperless-ngx/pull/8699))
* Enhancement: custom field sorting [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8494](https://github.com/paperless-ngx/paperless-ngx/pull/8494))

[1.31.1]
* Update paperless-ngx to 2.14.2
* [Full Changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.13.2)
* Fix: dont try to parse empty webhook params [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8742](https://github.com/paperless-ngx/paperless-ngx/pull/8742))
* Fix: pass working file to workflows, pickle file bytes [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8741](https://github.com/paperless-ngx/paperless-ngx/pull/8741))
* Fix: use hard delete when bulk editing custom fields [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8740](https://github.com/paperless-ngx/paperless-ngx/pull/8740))
* Fix: Ensure email attachments use the latest document path for attachments [@&#8203;stumpylog](https://github.com/stumpylog) ([#&#8203;8737](https://github.com/paperless-ngx/paperless-ngx/pull/8737))
* Fix: include tooltip module for custom fields display [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8739](https://github.com/paperless-ngx/paperless-ngx/pull/8739))
* Fix: remove id of webhook/email actions on copy [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8729](https://github.com/paperless-ngx/paperless-ngx/pull/8729))
* Fix: import dnd module for merge confirm dialog [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8727](https://github.com/paperless-ngx/paperless-ngx/pull/8727))
* Chore(deps): Bump django from 5.1.4 to 5.1.5 [@&#8203;dependabot](https://github.com/dependabot) ([#&#8203;8738](https://github.com/paperless-ngx/paperless-ngx/pull/8738))
* Fix: dont try to parse empty webhook params [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8742](https://github.com/paperless-ngx/paperless-ngx/pull/8742))

[1.31.2]
* Update paperless-ngx to 2.14.3
* [Full Changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.13.2)
* Fix: Adds a default 30s timeout for emails, instead of no timeout [@&#8203;stumpylog](https://github.com/stumpylog) ([#&#8203;8757](https://github.com/paperless-ngx/paperless-ngx/pull/8757))
* Fix: import forms modules for entries component [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8752](https://github.com/paperless-ngx/paperless-ngx/pull/8752))
* Fix: fix email/wh actions on consume started [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8750](https://github.com/paperless-ngx/paperless-ngx/pull/8750))
* Fix: import date picker module in cf query dropdown [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8749](https://github.com/paperless-ngx/paperless-ngx/pull/8749))
* Fix: Adds a default 30s timeout for emails, instead of no timeout [@&#8203;stumpylog](https://github.com/stumpylog) ([#&#8203;8757](https://github.com/paperless-ngx/paperless-ngx/pull/8757))

[1.31.3]
* Update paperless-ngx to 2.14.4
* [Full Changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.13.2)
* Enhancement: allow specifying JSON encoding for webhooks [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8799](https://github.com/paperless-ngx/paperless-ngx/pull/8799))
* Change: disable API basic auth if MFA enabled [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8792](https://github.com/paperless-ngx/paperless-ngx/pull/8792))
* Fix: Include email and webhook objects in the export [@&#8203;stumpylog](https://github.com/stumpylog) ([#&#8203;8790](https://github.com/paperless-ngx/paperless-ngx/pull/8790))
* Fix: use MIMEBase for email attachments [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8762](https://github.com/paperless-ngx/paperless-ngx/pull/8762))
* Fix: handle page out of range in mgmt lists after delete [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8771](https://github.com/paperless-ngx/paperless-ngx/pull/8771))
* Enhancement: allow specifying JSON encoding for webhooks [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8799](https://github.com/paperless-ngx/paperless-ngx/pull/8799))
* Change: disable API basic auth if MFA enabled [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8792](https://github.com/paperless-ngx/paperless-ngx/pull/8792))
* Fix: Include email and webhook objects in the export [@&#8203;stumpylog](https://github.com/stumpylog) ([#&#8203;8790](https://github.com/paperless-ngx/paperless-ngx/pull/8790))
* Fix: use MIMEBase for email attachments [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8762](https://github.com/paperless-ngx/paperless-ngx/pull/8762))
* Fix: handle page out of range in mgmt lists after delete [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8771](https://github.com/paperless-ngx/paperless-ngx/pull/8771))

[1.31.4]
* Update paperless-ngx to 2.14.5
* [Full Changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.13.2)
* Change: restrict altering and creation of superusers to superusers only [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8837](https://github.com/paperless-ngx/paperless-ngx/pull/8837))
* Fix: fix long tag visual wrapping [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8833](https://github.com/paperless-ngx/paperless-ngx/pull/8833))
* Fix: Enforce classifier training ordering to prevent extra training [@&#8203;stumpylog](https://github.com/stumpylog) ([#&#8203;8822](https://github.com/paperless-ngx/paperless-ngx/pull/8822))
* Fix: import router module to not found component [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8821](https://github.com/paperless-ngx/paperless-ngx/pull/8821))
* Fix: better reflect some mail account / rule permissions in UI [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8812](https://github.com/paperless-ngx/paperless-ngx/pull/8812))
* Chore(deps-dev): Bump undici from 5.28.4 to 5.28.5 in /src-ui @&#8203;[dependabot\[bot\]](https://github.com/apps/dependabot) ([#&#8203;8851](https://github.com/paperless-ngx/paperless-ngx/pull/8851))
* Chore(deps-dev): Bump the development group with 2 updates @&#8203;[dependabot\[bot\]](https://github.com/apps/dependabot) ([#&#8203;8841](https://github.com/paperless-ngx/paperless-ngx/pull/8841))
* Chore(deps-dev): Bump undici from 5.28.4 to 5.28.5 in /src-ui @&#8203;[dependabot\[bot\]](https://github.com/apps/dependabot) ([#&#8203;8851](https://github.com/paperless-ngx/paperless-ngx/pull/8851))

[1.31.5]
* Update paperless-ngx to 2.14.6
* [Full Changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.14.6)
* Fix: backwards-compatible versioned API response for custom field select fields, update default API version [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8912](https://github.com/paperless-ngx/paperless-ngx/pull/8912))
* Tweak: place items with 0 documents at bottom of filterable list, retain alphabetical [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8924](https://github.com/paperless-ngx/paperless-ngx/pull/8924))
* Fix: set larger page size for abstract service getFew [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8920](https://github.com/paperless-ngx/paperless-ngx/pull/8920))
* Fix/refactor: remove doc observables, fix username async [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8908](https://github.com/paperless-ngx/paperless-ngx/pull/8908))
* Fix: include missing fields for saved view widgets [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8905](https://github.com/paperless-ngx/paperless-ngx/pull/8905))
* Fix: force set document not dirty before close after save [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8888](https://github.com/paperless-ngx/paperless-ngx/pull/8888))
* Fixhancement: restore search highlighting and add for built-in viewer [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8885](https://github.com/paperless-ngx/paperless-ngx/pull/8885))
* Fix: resolve cpu usage due to incorrect interval use [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8884](https://github.com/paperless-ngx/paperless-ngx/pull/8884))
* Fix: backwards-compatible versioned API response for custom field select fields, update default API version [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8912](https://github.com/paperless-ngx/paperless-ngx/pull/8912))
* Tweak: place items with 0 documents at bottom of filterable list, retain alphabetical [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8924](https://github.com/paperless-ngx/paperless-ngx/pull/8924))
* Fix: set larger page size for abstract service getFew [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8920](https://github.com/paperless-ngx/paperless-ngx/pull/8920))
* Fix/refactor: remove doc observables, fix username async [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8908](https://github.com/paperless-ngx/paperless-ngx/pull/8908))
* Chore: Upgrades dependencies and hook versions [@&#8203;stumpylog](https://github.com/stumpylog) ([#&#8203;8895](https://github.com/paperless-ngx/paperless-ngx/pull/8895))
* Fix: include missing fields for saved view widgets [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8905](https://github.com/paperless-ngx/paperless-ngx/pull/8905))
* Fix: force set document not dirty before close after save [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8888](https://github.com/paperless-ngx/paperless-ngx/pull/8888))
* Change: Revert dropdown sorting by doc count [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8887](https://github.com/paperless-ngx/paperless-ngx/pull/8887))
* Fixhancement: restore search highlighting and add for built-in viewer [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8885](https://github.com/paperless-ngx/paperless-ngx/pull/8885))
* Fix: resolve cpu usage due to incorrect interval use [@&#8203;shamoon](https://github.com/shamoon) ([#&#8203;8884](https://github.com/paperless-ngx/paperless-ngx/pull/8884))

[1.31.6]
* Update gotenberg to 8.16.0

[1.31.7]
* Update paperless-ngx to 2.14.7
* [Full Changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.14.7)
* Enhancement: require totp code for obtain auth token by [@&#8203;shamoon](https://github.com/shamoon) in https://github.com/paperless-ngx/paperless-ngx/pull/8936
* Enhancement: require totp code for obtain auth token by [@&#8203;shamoon](https://github.com/shamoon) in https://github.com/paperless-ngx/paperless-ngx/pull/8936
* Fix: reflect doc links in bulk modify custom fields by [@&#8203;shamoon](https://github.com/shamoon) in https://github.com/paperless-ngx/paperless-ngx/pull/8962
* Fix: also ensure symmetric doc link removal on bulk edit by [@&#8203;shamoon](https://github.com/shamoon) in https://github.com/paperless-ngx/paperless-ngx/pull/8963
* Chore(deps-dev): Bump ruff from 0.9.2 to 0.9.3 in the development group by [@&#8203;dependabot](https://github.com/dependabot) in https://github.com/paperless-ngx/paperless-ngx/pull/8928
* Enhancement: require totp code for obtain auth token by [@&#8203;shamoon](https://github.com/shamoon) in https://github.com/paperless-ngx/paperless-ngx/pull/8936
* Fix: reflect doc links in bulk modify custom fields by [@&#8203;shamoon](https://github.com/shamoon) in https://github.com/paperless-ngx/paperless-ngx/pull/8962
* Fix: also ensure symmetric doc link removal on bulk edit by [@&#8203;shamoon](https://github.com/shamoon) in https://github.com/paperless-ngx/paperless-ngx/pull/8963

[1.32.0]
* Update gotenberg to 8.17.0
* [Full Changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.14.7)
* Updates Chromium to version `133.0.6943.53` (except for `arm64`).
* Updates Go dependencies.

[1.32.1]
* Update gotenberg to 8.17.1
* [Full Changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.14.7)

[1.32.2]
* chore(deps): pin dependencies

[1.32.3]
* Update gotenberg to 8.17.2
* [Full Changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.14.7)
* The split feature does not sporadically return corrupt files anymore when using multiple engines. Thanks to [@&#8203;ninjacarr](https://github.com/ninjacarr) for the heads-up!
* Warnings from QPDF  do not return errors anymore. Thanks to [@&#8203;zshehov](https://github.com/zshehov) for the fix!
* Updates Chromium to version `134.0.6998.35` on the `amd64` platform.
* Updates Go dependencies.

[1.32.4]
* Update gotenberg to 8.17.3
* [Full Changelog](https://github.com/paperless-ngx/paperless-ngx/releases/tag/v2.14.7)
* Enhancement: require totp code for obtain auth token by @shamoon in #8936
* Enhancement: require totp code for obtain auth token by @shamoon in #8936
* Fix: reflect doc links in bulk modify custom fields by @shamoon in #8962
* Fix: also ensure symmetric doc link removal on bulk edit by @shamoon in #8963

