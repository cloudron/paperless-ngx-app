# see also TIKA_VERSION below
ARG TIKA_IMAGE_VERSION=2.9.2.1
ARG GOTENBERG_VERSION=8.17.3@sha256:779d4cf9bb36cb645e26e03fdb1b929b5d0fd01bfa58c336da5e224dd154626e

FROM apache/tika:${TIKA_IMAGE_VERSION}-full AS tika
FROM gotenberg/gotenberg:${GOTENBERG_VERSION} AS Gotenberg

FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# https://github.com/paperless-ngx/paperless-ngx/blob/main/docs/setup.rst#bare-metal-route
RUN apt-get update && \
    apt-get install -y \
        optipng gnupg gnupg2 libpoppler-cpp-dev libmagic-dev unpaper libreoffice ghostscript icc-profiles-free exiftool qpdf \
        liblept5 jbig2dec pngquant tesseract-ocr tesseract-ocr-all python3-setuptools python3-wheel libxml2 \
        openjdk-17-jre-headless gdal-bin tesseract-ocr tesseract-ocr-eng tesseract-ocr-ita tesseract-ocr-fra tesseract-ocr-spa tesseract-ocr-deu \
        zlib1g libzbar0 poppler-utils mime-support libpq-dev && \
    apt-get clean && \
    rm -r /var/cache/apt /var/lib/apt/lists

# renovate: datasource=github-releases depName=paperless-ngx/paperless-ngx versioning=semver extractVersion=^v(?<version>.+)$
ARG PAPERLESS_VERSION=2.14.7

RUN curl -L https://github.com/paperless-ngx/paperless-ngx/releases/download/v${PAPERLESS_VERSION}/paperless-ngx-v${PAPERLESS_VERSION}.tar.xz | tar -xJ -C /app/code/ --strip-components 1 -f -

RUN python3 -m pip install --upgrade pip
RUN pip3 install -r requirements.txt --no-cache-dir --ignore-installed pyyaml
RUN python3 ./src/manage.py collectstatic --clear --no-input && \
    python3 ./src/manage.py compilemessages
# https://github.com/paperless-ngx/paperless-ngx/blob/f9ce4d8f6a9086d21f7f9c5411a28dd8b0b7135e/Dockerfile#L233
# https://github.com/paperless-ngx/paperless-ngx/blob/454264a87f59a354546f487eba258cb01a364b55/src/paperless/settings.py#L181
RUN python3 -W ignore::RuntimeWarning -m nltk.downloader -d "/usr/share/nltk_data" snowball_data \
    && python3 -W ignore::RuntimeWarning -m nltk.downloader -d "/usr/share/nltk_data" stopwords \
    && python3 -W ignore::RuntimeWarning -m nltk.downloader -d "/usr/share/nltk_data" punkt_tab

# Apache Tika
ARG TIKA_VERSION=2.9.2
COPY --from=tika /tika-server-standard-${TIKA_VERSION}.jar /tika-server-standard.jar
RUN ln -sf /app/data/tika-extras /tika-extras

# GOTENBERG
COPY --from=gotenberg /usr/bin/gotenberg /usr/bin/
COPY --from=gotenberg /etc/fonts/conf.d/100-gotenberg.conf /etc/fonts/conf.d/100-gotenberg.conf
COPY --from=gotenberg /usr/bin/pdfcpu /usr/bin/

# Install fonts https://github.com/gotenberg/gotenberg/blob/main/build/Dockerfile#L67
RUN curl -o ./ttf-mscorefonts-installer_3.8.1_all.deb http://httpredir.debian.org/debian/pool/contrib/m/msttcorefonts/ttf-mscorefonts-installer_3.8.1_all.deb && \
    apt-get update -qq && \
    apt-get install -y -qq --no-install-recommends ./ttf-mscorefonts-installer_3.8.1_all.deb xfonts-utils culmus fonts-freefont-ttf fonts-beng fonts-hosny-amiri fonts-lklug-sinhala fonts-lohit-guru fonts-lohit-knda fonts-samyak-gujr fonts-samyak-mlym fonts-samyak-taml fonts-sarai fonts-sil-abyssinica fonts-sil-padauk fonts-telu fonts-thai-tlwg ttf-wqy-zenhei fonts-arphic-ukai fonts-arphic-uming fonts-ipafont-mincho fonts-ipafont-gothic fonts-unfonts-core \
    fonts-crosextra-caladea fonts-crosextra-carlito fonts-dejavu fonts-dejavu-extra fonts-liberation fonts-liberation2 fonts-linuxlibertine fonts-noto-cjk fonts-noto-core fonts-noto-mono fonts-noto-ui-core \
    fonts-sil-gentium fonts-sil-gentium-basic && \
    rm -f ./ttf-mscorefonts-installer_3.8.1_all.deb && \
    # Add Color and Black-and-White Noto emoji font.
    curl -Ls "https://github.com/googlefonts/noto-emoji/raw/v2.047/fonts/NotoColorEmoji.ttf" -o /usr/local/share/fonts/NotoColorEmoji.ttf &&\
    # Cleanup.
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install Google Chrome stable
RUN curl https://dl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    echo "deb http://dl.google.com/linux/chrome/deb/ stable main" | tee /etc/apt/sources.list.d/google-chrome.list && \
    apt-get update -qq && \
    apt-get install -y -qq --no-install-recommends --allow-unauthenticated google-chrome-stable && \
    mv /usr/bin/google-chrome-stable /usr/bin/chromium && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install LibreOffice & unoconverter.
# unoconverter will look for the Python binary, which has to be at version 3.
RUN curl -Ls https://raw.githubusercontent.com/gotenberg/unoconverter/v0.0.1/unoconv -o /usr/bin/unoconverter && \
    ln -s /usr/bin/python3 /usr/bin/python && \
    chmod +x /usr/bin/unoconverter

# Install PDFtk
# See https://gitlab.com/pdftk-java/pdftk/-/releases - Binary package.
ARG PDFTK_VERSION=v3.3.3
RUN curl -o /usr/bin/pdftk-all.jar "https://gitlab.com/api/v4/projects/5024297/packages/generic/pdftk-java/$PDFTK_VERSION/pdftk-all.jar" && \
    chmod a+x /usr/bin/pdftk-all.jar && \
    echo -e '#!/bin/bash\n\nexec java -jar /usr/bin/pdftk-all.jar "$@"' > /usr/bin/pdftk && \
    chmod +x /usr/bin/pdftk && \
    # See https://github.com/nextcloud/docker/issues/380.
    mkdir -p /usr/share/man/man1 && \
    # Verify installations.
    pdftk --version && \
    qpdf --version && \
    exiftool --version

# Environment variables required by modules or else.
ENV CHROMIUM_BIN_PATH=/usr/bin/chromium
ENV LIBREOFFICE_BIN_PATH=/usr/lib/libreoffice/program/soffice.bin
ENV UNOCONVERTER_BIN_PATH=/usr/bin/unoconverter
ENV PDFTK_BIN_PATH=/usr/bin/pdftk
ENV QPDF_BIN_PATH=/usr/bin/qpdf
ENV EXIFTOOL_BIN_PATH=/usr/bin/exiftool
ENV PDFCPU_BIN_PATH=/usr/bin/pdfcpu

RUN rm -rf /app/code/data && ln -sf /app/data/data /app/code/data && \
    rm -rf /app/code/media && ln -sf /app/data/media /app/code/media && \
    rm -rf /app/code/consume && ln -sf /app/data/consume /app/code/consume && \
    ln -sf /app/data/paperless.conf /app/code/paperless.conf

ADD policy.xml /etc/ImageMagick-6

RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
ADD supervisor/* /etc/supervisor/conf.d/

ADD paperless.conf.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
