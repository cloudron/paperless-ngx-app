This app is pre-setup with an admin account.

The initial credentials are:

**Username**: admin<br/>
**Password**: changeme123<br/>
**Email**: admin@cloudron.local<br/>

<sso>
By default, Cloudron users have no permissions. Users will only see a Loading screen. This can be changed by the Paperless-ngx admin in the `Users & Groups` tab.

Minimal permissions to let users to login:
**Add UISettings**
**View UISettings**
</sso>
