#!/bin/bash

set -eu
cd /app/code

mkdir -p /run/paperless \
        /app/data/media/documents/originals \
        /app/data/media/documents/thumbnails \
        /app/data/consume \
        /tmp/paperless \
        /app/data/tika-extras

if [[ ! -f /app/data/.initialized ]]; then
    echo "=> Fresh installation, setting up data directory..."
    cp /app/pkg/paperless.conf.template /app/data/paperless.conf

    crudini --set /app/data/paperless.conf "" PAPERLESS_URL "${CLOUDRON_APP_ORIGIN}"
    crudini --set /app/data/paperless.conf "" PAPERLESS_REDIS "redis://${CLOUDRON_REDIS_HOST}:${CLOUDRON_REDIS_PORT}"
    crudini --set /app/data/paperless.conf "" PAPERLESS_DBHOST "${CLOUDRON_POSTGRESQL_HOST}"
    crudini --set /app/data/paperless.conf "" PAPERLESS_DBPORT "${CLOUDRON_POSTGRESQL_PORT}"
    crudini --set /app/data/paperless.conf "" PAPERLESS_DBNAME "${CLOUDRON_POSTGRESQL_DATABASE}"
    crudini --set /app/data/paperless.conf "" PAPERLESS_DBUSER "${CLOUDRON_POSTGRESQL_USERNAME}"
    crudini --set /app/data/paperless.conf "" PAPERLESS_DBPASS "${CLOUDRON_POSTGRESQL_PASSWORD}"

    echo "=> Setting initial app secret"
    SECRET=`date +%s|sha256sum|base64|head -c 32`
    crudini --set /app/data/paperless.conf "" PAPERLESS_SECRET_KEY "${SECRET}"

    echo "=> Running initial migration"
    cd /app/code/src && python3 manage.py migrate

    echo "=> Creating initial super user"
    export DJANGO_SUPERUSER_PASSWORD="changeme123"
    cd /app/code/src && python3 manage.py createsuperuser --username admin --email admin@cloudron.local --no-input

    touch /app/data/.initialized
fi


echo "=> Patching paperless.conf"
crudini --set /app/data/paperless.conf "" PAPERLESS_URL "${CLOUDRON_APP_ORIGIN}"
crudini --set /app/data/paperless.conf "" PAPERLESS_REDIS "redis://${CLOUDRON_REDIS_HOST}:${CLOUDRON_REDIS_PORT}"
crudini --set /app/data/paperless.conf "" PAPERLESS_DBHOST "${CLOUDRON_POSTGRESQL_HOST}"
crudini --set /app/data/paperless.conf "" PAPERLESS_DBPORT "${CLOUDRON_POSTGRESQL_PORT}"
crudini --set /app/data/paperless.conf "" PAPERLESS_DBNAME "${CLOUDRON_POSTGRESQL_DATABASE}"
crudini --set /app/data/paperless.conf "" PAPERLESS_DBUSER "${CLOUDRON_POSTGRESQL_USERNAME}"
crudini --set /app/data/paperless.conf "" PAPERLESS_DBPASS "${CLOUDRON_POSTGRESQL_PASSWORD}"

crudini --set /app/data/paperless.conf "" PAPERLESS_EMAIL_HOST "${CLOUDRON_MAIL_SMTP_SERVER}"
crudini --set /app/data/paperless.conf "" PAPERLESS_EMAIL_PORT "${CLOUDRON_MAIL_SMTP_PORT}"
crudini --set /app/data/paperless.conf "" PAPERLESS_EMAIL_HOST_USER "${CLOUDRON_MAIL_SMTP_USERNAME}"
crudini --set /app/data/paperless.conf "" PAPERLESS_EMAIL_HOST_PASSWORD "${CLOUDRON_MAIL_SMTP_PASSWORD}"
# https://docs.djangoproject.com/en/dev/ref/settings/#default-from-email
crudini --set /app/data/paperless.conf "" PAPERLESS_EMAIL_FROM "${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Paperless-ngx} <${CLOUDRON_MAIL_FROM}>"
crudini --set /app/data/paperless.conf "" PAPERLESS_EMAIL_USE_SSL "false"
crudini --set /app/data/paperless.conf "" PAPERLESS_EMAIL_USE_TLS "false"

# Tika & Gotenberg parsing and converting
crudini --set /app/data/paperless.conf "" PAPERLESS_TIKA_ENABLED "true"
crudini --set /app/data/paperless.conf "" PAPERLESS_TIKA_ENDPOINT "http://localhost:9998"
crudini --set /app/data/paperless.conf "" PAPERLESS_TIKA_GOTENBERG_ENDPOINT "http://localhost:3000"

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    crudini --set /app/data/paperless.conf "" PAPERLESS_APPS "allauth.socialaccount.providers.openid_connect"
    crudini --set /app/data/paperless.conf "" PAPERLESS_SOCIAL_AUTO_SIGNUP "true"
    crudini --set /app/data/paperless.conf "" PAPERLESS_SOCIALACCOUNT_ALLOW_SIGNUPS "true"
    crudini --set /app/data/paperless.conf "" PAPERLESS_SOCIALACCOUNT_PROVIDERS "{\"openid_connect\": {\"APPS\": [{\"provider_id\": \"cloudron\",\"name\": \"${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}\",\"client_id\": \"${CLOUDRON_OIDC_CLIENT_ID}\",\"secret\": \"${CLOUDRON_OIDC_CLIENT_SECRET}\",\"settings\": { \"server_url\": \"${CLOUDRON_OIDC_ISSUER}/.well-known/openid-configuration\"}}]}}"
fi

echo "=> Process migrations"
cd /app/code/src && python3 manage.py migrate

echo "=> Ensure permissions"
chown -Rh cloudron:cloudron /app/data /tmp/paperless

echo "=> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Paperlessng
