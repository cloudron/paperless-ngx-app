#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const adminUsername = 'admin';
    const adminPassword = 'changeme123';

    let browser, app, cloudronName;

    let username = process.env.USERNAME;
    let password = process.env.PASSWORD;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');

        const tmp = execSync(`cloudron exec --app ${app.id} env`).toString().split('\n').find((l) => l.indexOf('CLOUDRON_OIDC_PROVIDER_NAME=') === 0);
        if (tmp) cloudronName = tmp.slice('CLOUDRON_OIDC_PROVIDER_NAME='.length);
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password) {
        await browser.get(`https://${app.fqdn}/accounts/login/?next=/`);

        await waitForElement(By.id('inputUsername'));
        await browser.findElement(By.id('inputUsername')).sendKeys(username);
        await browser.findElement(By.id('inputPassword')).sendKeys(password);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.xpath(`//span[contains(text(),"${username}")]`));
    }

    async function loginOIDC(username, password, alreadyAuthenticated = true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/accounts/login/?next=/`);

        await waitForElement(By.xpath(`//button[contains(., "Cloudron") or contains(., "${cloudronName}")]`));
        await browser.findElement(By.xpath(`//button[contains(., "Cloudron") or contains(., "${cloudronName}")]`)).click();

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await browser.sleep(2000);
        // await waitForElement(By.xpath('//h3[contains(.,"Dashboard")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/accounts/logout/`);
        await browser.sleep(2000);
        await waitForElement(By.id('inputUsername'));
    }

    //    /* add permissions for:
    //    *   add_document
    //    *   change_document
    //    *   delete_document
    //    *   view_document
    //    *   add_documenttype
    //    *   change_documenttype
    //    *   delete_documenttype
    //    *   view_documenttype
    //    */
    //    execSync(`cloudron exec --app ${app.fqdn} -- bash -c 'PGPASSWORD=$CLOUDRON_POSTGRESQL_PASSWORD psql -h $CLOUDRON_POSTGRESQL_HOST -p $CLOUDRON_POSTGRESQL_PORT -U $CLOUDRON_POSTGRESQL_USERNAME -d $CLOUDRON_POSTGRESQL_DATABASE -c "WITH users AS (SELECT id AS user_id FROM auth_user WHERE username='\\'${username}\\''), permissions AS (SELECT id AS permission_id FROM auth_permission WHERE codename='\\'view_uisettings\\'' OR codename like '\\'%document%\\'') INSERT INTO auth_user_user_permissions (user_id, permission_id) SELECT user_id, permission_id FROM users, permissions ON CONFLICT (user_id, permission_id) DO NOTHING"'`, { encoding: 'utf8', stdio: 'inherit' });

    async function makeUserAdmin(username) {
        await browser.get(`https://${app.fqdn}/usersgroups`);
        await browser.sleep(2000);
        await waitForElement(By.xpath(`//button[text()="${username}"]`));
        await browser.findElement(By.xpath(`//button[text()="${username}"]`)).click();
        await browser.sleep(2000);
        // tick Superuser checkbox off if not selected
        if (await browser.findElements(By.xpath('//button[@type="submit" and contains(text(), "Sign up")]')).then(found => !found.Selected)) {
            await browser.findElement(By.xpath('//label[@for="is_superuser"]')).click();
            await browser.sleep(2000);
        }
        await browser.findElement(By.xpath('//button[text()="Save"]')).click();
        await browser.sleep(2000);
    }

    async function uploadDocument(fileName) {
        execSync(`cloudron push --app ${app.id} ./${fileName} /app/data/consume/${fileName}`);

        // Need to wait for consumer to consume sample PDF
        console.log('Sleeping for 60 seconds to allow consuming of document'); // cron runs every minute
        await browser.sleep(60000);
    }

    async function documentExists(docName) {
        await browser.get(`https://${app.fqdn}/documents`);
        await waitForElement(By.xpath('//p[contains(text(), "pdf-sample")]'));
    }

    xit('can build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // No SSO
    it('can install app (No SSO)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, adminUsername, adminPassword));
    it('can upload pdf document', uploadDocument.bind(null, 'pdf-sample.pdf'));
    it('pdf document exists', documentExists.bind(null, 'pdf-sample'));
    it('can upload odt document', uploadDocument.bind(null, 'odt-sample.odt'));
    it('document odt exists', documentExists.bind(null, 'odt-sample'));
    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // SSO
    it('can install app (SSO)', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login (OIDC)', loginOIDC.bind(null, username, password, false));
    it('can logout', logout);

    it('can admin login', login.bind(null, adminUsername, adminPassword));
    it('can make "' + username + '" Admin', makeUserAdmin.bind(null, username));
    it('can admin logout', logout);

    it('can login (OIDC)', loginOIDC.bind(null, username, password, true));
    it('can upload document', uploadDocument.bind(null, 'pdf-sample.pdf'));
    it('document exists', documentExists.bind(null, 'pdf-sample'));
    it('can logout', logout);

    it('restart app', function() { execSync('cloudron restart --app ' + app.id); });

    it('can login (OIDC)', loginOIDC.bind(null, username, password, true));
    it('document exists', documentExists.bind(null, 'pdf-sample'));
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', function () { execSync('cloudron restore --app ' + app.id, EXEC_ARGS); });

    it('can login (OIDC)', loginOIDC.bind(null, username, password, true));
    it('document exists', documentExists.bind(null, 'pdf-sample'));
    it('can logout', logout);

    it('move to different location', async function () {
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login (OIDC)', loginOIDC.bind(null, username, password));
    it('document exists', documentExists.bind(null, 'pdf-sample'));

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id com.paperlessng.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can get app information', getAppInfo);
    it('can login (OIDC)', loginOIDC.bind(null, username, password, true));
    it('can logout', logout);

    it('can admin login', login.bind(null, adminUsername, adminPassword));
    it('can make "' + username + '" Admin', makeUserAdmin.bind(null, username));
    it('can admin logout', logout);

    it('can login (OIDC)', loginOIDC.bind(null, username, password, true));
    it('can upload pdf document', uploadDocument.bind(null, 'pdf-sample.pdf'));
    it('pdf document exists', documentExists.bind(null, 'pdf-sample'));
    it('can upload odt document', uploadDocument.bind(null, 'odt-sample.odt'));
    it('document odt exists', documentExists.bind(null, 'odt-sample'));
    it('can logout', logout);

    it('can update', async function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(5000);
    });

    it('can admin login', login.bind(null, adminUsername, adminPassword));
    it('can admin logout', logout);

    it('can login (OIDC)', loginOIDC.bind(null, username, password, true));
    it('document exists', documentExists.bind(null, 'pdf-sample'));
    it('document odt exists', documentExists.bind(null, 'odt-sample'));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
